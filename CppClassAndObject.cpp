
#include <iostream>
#include<cmath>

using namespace std;

class Vector {
private:
    double x;
    double y;
    double z;
public:
    Vector() : x(0), y(0), z(0) {}
    Vector(double x, double y, double z) {
        this->x = x;
        this->y = y;
        this->z = z;
    }

    void ShowVector() {
        cout << "x = " << x << "\n";
        cout << "y = " << y << "\n";
        cout << "z = " << z << "\n";
    }

    double VectorModul() {
        return sqrt(pow(x,2) + pow(y, 2) + pow(z, 2));
    }
};



int main()
{
    Vector vec;
    vec.ShowVector();

    Vector vec2(12, 23, 43);
    vec2.ShowVector();

    cout << vec2.VectorModul() << "\n";
}